//
//  Container+ViewModels.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 18/11/2021.
//


import Foundation
import Swinject
import SwinjectAutoregistration

extension Container {
    func registerViewModels() {

        autoregister(ApiDataNetworkConfig.self, initializer: ApiDataNetworkConfig.init)
        
        autoregister(RemoteDataSource.self, initializer: RemoteDataSource.init)
   
        autoregister(UserStorage.self, initializer: DefaultUserStorage.init)
        
        autoregister(UserRepository.self, initializer: DefaultUserRepository.init)
        
        
     
        
        autoregister(SearchOnUserViewModel.self, initializer: SearchOnUserViewModel.init)

        autoregister(FollowersViewModel.self, initializer: FollowersViewModel.init)

       
        
    }
}
