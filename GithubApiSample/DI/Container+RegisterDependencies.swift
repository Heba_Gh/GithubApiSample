//
//  RegisterDependencies.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 18/11/2021.
//


import Foundation
import Swinject

extension Container {
    func registerDependencies() {
  
        registerViewModels()
        registerUseCases()
    }
}
