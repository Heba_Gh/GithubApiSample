//
//  UserRepository.swift
//  GitHubApp
//
//  Created by Mohammed Jarkas on 18/11/2021.
//

import Foundation

final class DefaultUserRepository {
    private let config: ApiDataNetworkConfig
    private let remoteDataSource : RemoteDataSource
    private let userStorage : UserStorage
    init( config : ApiDataNetworkConfig , remoteDataSource : RemoteDataSource , userStorage : UserStorage) {
        self.config = config
        self.remoteDataSource = remoteDataSource
        self.userStorage = userStorage
    }
}

extension DefaultUserRepository: UserRepository {
   
    func fetchUser(with name : String ,  cached: @escaping (UserObject) -> Void, completion: @escaping (Result<User>) -> Void) {
        
        if let user =  userStorage.loadUser(name)
        {
            print(user.name)
            cached(user)
          //  completion(.success(data: user , code: 200 , message: ""))
        } // check if there internet connection
        else if isOnline
        {
        let endpoint = APIEndpoints.getUser(with: name)
        do
        {
            
            let ( urlRequest ,  parameter) = try endpoint.urlRequest(with: config)
            remoteDataSource.sendRequest( urlRequest, param: parameter  , completion: {
                (result: Result<User>) in
                switch result
                {
                case .success( let data , let code , let message):
                   
                   
                    self.userStorage.saveUser(data.managedObject())
                     completion(.success(data: data , code: code , message: message))
                     
                    break
                case .failure(let error):
                    completion(.failure(error))
                    break
                case .loading:
                    completion(.loading)
                    break
                }
            })
        }
        catch {
            completion(.failure(AppError(reason: .urlGeneration, message: "check your url")))
            print("urlGeneration")
        }
        }
        else{
            completion(.failure(AppError(reason: .networkError, message: "Please check your internet connection")))
        }
    }
    
    public func fetchFollowersList(with name : String , page : Int  , completion: @escaping (Result<[Follower]>) -> Void)
    {
      
        if !isOnline
        {
            completion(.failure(AppError(reason: .networkError, message: "Please check your internet connection")))
            return
        }
        let requestParameter = FollowRequestValue( page: page, per_page: per_page)
        let endpoint = APIEndpoints.getUserFollowers(with: name, followersRequest: requestParameter)
        do
        {
            
            let ( urlRequest ,  parameter) = try endpoint.urlRequest(with: config)
            remoteDataSource.sendRequest( urlRequest, param: parameter  , completion: {
                (result: Result<[Follower]>) in
                switch result
                {
                case .success( let data , let code , let message):
                    
                   
                        completion(.success(data: data , code: code , message: message))
                    
                    break
                case .failure(let error):
                    completion(.failure(error))
                    break
                case .loading:
                    completion(.loading)
                    break
                }
            })
        }
        catch {
            completion(.failure(nil))
            print("urlGeneration")
        }
     
    }
  
    public func fetchFollowingList(with name : String , page : Int  , completion: @escaping (Result<[Follower]>) -> Void)
    {
      
        let requestParameter = FollowRequestValue( page: page, per_page: per_page)
        let endpoint = APIEndpoints.getUserFollowing(with: name, followersRequest: requestParameter)
        do
        {
            
            let ( urlRequest ,  parameter) = try endpoint.urlRequest(with: config)
            remoteDataSource.sendRequest( urlRequest, param: parameter  , completion: {
                (result: Result<[Follower]>) in
                switch result
                {
                case .success( let data , let code , let message):
                     completion(.success(data: data , code: code , message: message))
                    
                    break
                case .failure(let error):
                    completion(.failure(error))
                    break
                case .loading:
                    completion(.loading)
                    break
                }
            })
        }
        catch {
            completion(.failure(nil))
            print("urlGeneration")
        }
     
    }
}
