//
//  FollowRequestValue.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 23/11/2021.
//

import Foundation
struct FollowRequestValue  : Encodable
{
        let page: Int
        let per_page : Int
}
