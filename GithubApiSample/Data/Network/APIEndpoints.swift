//
//  APIEndpoints.swift
//  GitHubApp
//
//  Created by Mohammed Jarkas on 18/11/2021.
//

import Foundation

struct APIEndpoints
{
    
    static func getUser(with name: String) -> Endpoint {

        return Endpoint(path: "users/\(name)",
                        method: .get )
    }

    static func getUserFollowers(with name : String , followersRequest : FollowRequestValue) -> Endpoint
    {
        return Endpoint(path: "users/\(name)/followers",
                        method: .get , queryParametersEncodable: followersRequest)
    }
    
    static func getUserFollowing(with name : String , followersRequest : FollowRequestValue) -> Endpoint
    {
        return Endpoint(path: "users/\(name)/following",
                        method: .get , queryParametersEncodable: followersRequest)
    }
}



