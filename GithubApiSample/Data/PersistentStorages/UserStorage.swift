//
//  Storage.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 21/11/2021.
//

import RealmSwift

protocol UserStorage
{
    func saveUser(_ user: UserObject)
    func loadUser(_ name : String) -> UserObject?
}
class DefaultUserStorage : UserStorage
{
    var realm: Realm = try! Realm()
    
    // SAVE USER IN DATABASE
    public func saveUser(_ user: UserObject)
    {
        try! realm.write
        {
            realm.add(user)
        }
    }
    
    // GET USER FROM DATABASE
    public func loadUser(_ name : String) -> UserObject?
    {
        let predicate = NSPredicate(format: "login = %@ OR login = %@", name.lowercased() , name.uppercased())
        return realm.objects(UserObject.self).filter(predicate).last
    }
}
