//
//  UserObject.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 22/11/2021.
//

import Foundation
import RealmSwift

class UserObject: Object {
    
    @objc dynamic var id: Int = 0
   
    @objc dynamic var login: String = ""
    @objc dynamic var avatarURL: String = ""
    
   
    @objc dynamic var type: String = ""
   
    @objc dynamic var name :String = ""
    @objc dynamic var company: String = ""
   
    @objc dynamic var location: String = ""
   
    @objc dynamic var bio : String = ""
    @objc dynamic var followers : Int = 0
    @objc dynamic var following: Int = 0
  
    // search on user by query
    @objc dynamic var query: String = ""
    
    func toUser() -> User
    {
        return User(id: id, login: login, name: name, company: company, avatarURL: avatarURL, type: type, location: location, bio: bio, followers: followers, following: following)
    }
    
}
