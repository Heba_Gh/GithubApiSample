//
//  CustomErrorView.swift
//  twofour54-ios
//
//  Created by Rama Shaaban on 25/05/2021.
//

import Foundation
import UIKit

protocol CustomErrorViewDelegate  {
    func didTapRetry()
}

class CustomErrorView: UIView {
    
    @IBOutlet weak var errorTextLabel: UILabel!
    @IBOutlet weak var submitView: CardView!
  
    
    var delegate: CustomErrorViewDelegate?
    
 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        submitView.delegate = self
    }
}


extension CustomErrorView: CardViewDelegate {
    func didTap(cardView: CardView) {
        self.delegate?.didTapRetry()
    }
    
    
}
