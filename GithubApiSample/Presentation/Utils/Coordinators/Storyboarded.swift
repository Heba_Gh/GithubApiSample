//
//  Storyboarded.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 18/11/2021.
//

import Foundation
import UIKit
enum StoryboardName
{
    case Main
    case Dashboard
    case Flights
    
    var description : String {
       switch self {
       // Use Internationalization, as appropriate.
       case .Main: return "Main"
       case .Dashboard: return "Dashboard"
       case .Flights: return "Flights"
       }
     }
}
protocol Storyboarded {
    static func instantiate(storyboard : StoryboardName) -> Self
}

extension Storyboarded where Self: UIViewController {
    static func instantiate(storyboard : StoryboardName = .Main) -> Self {
        // this pulls out "MyApp.MyViewController"
        let fullName = NSStringFromClass(self)

        // this splits by the dot and uses everything after, giving "MyViewController"
        let className = fullName.components(separatedBy: ".")[1]

        // load our storyboard
        let storyboard = UIStoryboard(name: storyboard.description, bundle: Bundle.main)

        // instantiate a view controller with that identifier, and force cast as the type that was requested
        return storyboard.instantiateViewController(withIdentifier: className) as! Self
    }
}
