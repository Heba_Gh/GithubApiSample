//
//  MainCoordinator.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 18/11/2021.
//

import Foundation
import UIKit


class MainCoordinator: Coordinator {
   
    var navigationController: UINavigationController

    static var shared : MainCoordinator?
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
    func pop() {
        navigationController.popViewController(animated: true)
    }
    

    func push(destination: Destination) {
        
        switch destination
        {
      
        case .search:
            let vc = SearchOnUserViewController(nibName: "SearchOnUserViewController", bundle: nil)
            vc.viewModel = AppDelegate.container.resolve(SearchOnUserViewModel.self)
            self.navigationController.pushViewController(vc, animated: true)
      
        default:
            break
        }
    }

    func pushFollowPage(with name : String , followType : FollowerType )
    {
        let vc = FollowViewController(nibName: "FollowViewController", bundle: nil)
        vc.viewModel = AppDelegate.container.resolve(FollowersViewModel.self)
        vc.viewModel.userName = name
        vc.viewModel.followType = followType
        self.navigationController.pushViewController(vc, animated: true)
    }
}
