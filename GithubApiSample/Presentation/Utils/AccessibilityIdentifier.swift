//
//  AccessibilityIdentifier.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 21/11/2021.
//
import Foundation

public struct AccessibilityIdentifier {
  
    static let searchField = "AccessibilityIdentifierSearchUser"
}
