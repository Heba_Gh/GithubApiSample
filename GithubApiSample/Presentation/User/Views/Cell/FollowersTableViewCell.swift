//
//  RectangleMealTableViewCell.swift
//  sefertasi-ios
//
//  Created by Rama on 16/12/2020.
//

import UIKit
import Kingfisher

class FollowersTableViewCell: UITableViewCell {

    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var name: UILabel!
   
    @IBOutlet weak var userType: UILabel!
    
    private var user : Follower!
    {
        didSet
        {
            name.text = "\(user.login)"
            userType.text = "account type: \(user.type!)"
            if let url = user.avatarURL
            {
                userImage.kf.indicatorType = .activity
                
                userImage.kf.setImage(with: URL(string: url) )
                
            }

        }
    }
    
    func configure(user : Follower)
    {
        self.user = user
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()


    }


}
