

import UIKit
import RxSwift
import Kingfisher
class SearchOnUserViewController: BaseViewController {
 
   
    var viewModel : SearchOnUserViewModel!
    let disposeBag = DisposeBag()
    
    @IBOutlet private var searchBarContainer: UIView!
    
    @IBOutlet weak var followingCardView: CardView!
    @IBOutlet weak var followersCardView: CardView!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var conatinerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    //  @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    private var searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupViews()
        viewModel = AppDelegate.container.resolve(SearchOnUserViewModel.self)
    }


    private func setupViews() {
        followingCardView.delegate = self
        followersCardView.delegate = self
        conatinerView.isHidden = true
        setupSearchController()
    }
     
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchController.isActive = false
    }
    

    func loadData()
    {
        conatinerView.isHidden = false
        if let user = viewModel.user
        {
        nameLabel.text = user.name
        if let url = user.avatarURL
        {
            profileImage.kf.indicatorType = .activity

            profileImage.kf.setImage(with: URL(string: url))

        }
            bioLabel.text = user.bio
            followingLabel.text = "\(user.following!) following"
            followersLabel.text = "\(user.followers!) followers"
            locationLabel.text = user.location
        }
    }
    
    
}

// MARK: - Search Controller

extension SearchOnUserViewController {
    private func setupSearchController() {
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "search on user"
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.translatesAutoresizingMaskIntoConstraints = true
        searchController.searchBar.barStyle = .black
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.frame = searchBarContainer.bounds
        searchController.searchBar.autoresizingMask = [.flexibleWidth]
        searchBarContainer.addSubview(searchController.searchBar)
        definesPresentationContext = true
        if #available(iOS 13.0, *) {
            searchController.searchBar.searchTextField.accessibilityIdentifier = AccessibilityIdentifier.searchField
        }
    }
}

extension SearchOnUserViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text, !searchText.isEmpty else { return }
    //    searchController.isActive = false
        let observable = viewModel.search(with: searchText)
         observable.subscribe(onNext: {
              result in
              switch result
              {
              case .loading:
                  self.showFullScreenLoading()
                  break
              case .failure(let error):
                  self.finishFullScreenLoading()
                  messageView.showWarningMessage(message: error?.message ?? "unknown error")
                  break
              case .success(_, _ , _):
                  self.finishFullScreenLoading()
                  self.loadData()
               
                  break
              }
          }).disposed(by: disposeBag)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
     //   viewModel.didCancelSearch()
    }
}
extension SearchOnUserViewController: UISearchControllerDelegate {
    public func willPresentSearchController(_ searchController: UISearchController) {
        conatinerView.isHidden = true
    }

    public func willDismissSearchController(_ searchController: UISearchController) {
      
    }

    public func didDismissSearchController(_ searchController: UISearchController) {
       
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        conatinerView.isHidden = true
        return true
    }
}
extension SearchOnUserViewController : CardViewDelegate
{
    func didTap(cardView: CardView)
    {
        switch cardView
        {
        case followersCardView:
            if viewModel.user?.followers ?? 0 > 0
            {
            MainCoordinator.shared?.pushFollowPage(with: viewModel.user?.login ?? "" , followType: .followers)
            }
            break
        case followingCardView :
            if viewModel.user?.following  ?? 0 > 0
            {
            MainCoordinator.shared?.pushFollowPage(with: viewModel.user?.login ?? "" , followType: .following)
            }
            break
        default:
            break
        }
        
    }
    
    
}
