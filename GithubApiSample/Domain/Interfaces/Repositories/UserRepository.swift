

import Foundation
protocol UserRepository
{
    func fetchUser(with name : String , cached: @escaping (UserObject) -> Void , completion: @escaping (Result<User>) -> Void)
    
    func fetchFollowersList(with name : String , page : Int ,  completion: @escaping (Result<[Follower]>) -> Void)
    
    func fetchFollowingList(with name : String , page : Int ,  completion: @escaping (Result<[Follower]>) -> Void)
}

