

import Foundation
import RxSwift
protocol GetUserProfileUseCase
{
    func execute(with name: String) -> Observable<Result<User>>
}

final class DefaultGetUserProfileUseCase : GetUserProfileUseCase {
    
    private let defaultRepository: UserRepository
    
    
    init(defaultRepository: UserRepository)
    {
        self.defaultRepository = defaultRepository
    }
    
    func execute(with name: String) -> Observable<Result<User>> {
        
        return Observable<Result<User>>.create { observer in
           
            observer.onNext(Result.loading)
                self.defaultRepository.fetchUser(with: name,
                cached: {
                    userObject in
                    let user = userObject.toUser()
                    observer.onNext(Result.success(data: user, code: 200, message: "success"))
                    // convert Realm Object to User Struct
                }, completion: { result in
                
                observer.onNext(result)
            })
            
            return Disposables.create()
        }
    }
}

