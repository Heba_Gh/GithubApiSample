//
//  GetFollowingUseCase.swift
//  GithubApiSample
//
//  Created by Mohammed Jarkas on 23/11/2021.
//
import Foundation
import RxSwift
protocol GetFollowingUseCase {
    func execute(with name : String , page: Int) -> Observable<Result<[Follower]>>
}

final class DefaultGetFollowingUseCase: GetFollowingUseCase {
    
    private let defaultRepository: UserRepository
    
    
    init(defaultRepository: UserRepository)
    {
        self.defaultRepository = defaultRepository
    }
    
    func execute(with name : String , page: Int) -> Observable<Result<[Follower]>> {
        
        return Observable<Result<[Follower]>>.create { observer in
            if !isOnline
            {
                observer.onNext(Result.failure(AppError(reason: .networkError, message: "Please check your internet connection")))
            }
            else
            {
            observer.onNext(Result.loading)
            
                self.defaultRepository.fetchFollowingList(with: name, page: page, completion: { result in
                
                observer.onNext(result)
            })
            }
            return Disposables.create()
        }
    }
}
    
