

import Foundation

struct ErrorResponse: Codable ,  Equatable {
   
    
    let message : String?
   
  
    
    enum CodingKeys: String, CodingKey {
        case  message
        
      
    }
}

// MARK: - User
struct User: Codable ,  Equatable {
   
    let id: Int
    let login : String
    let name : String?
    let company: String?
    let avatarURL: String?
    let type: String?
    let location: String?
    let bio : String?
    let followers : Int?
    let following: Int?
  
    
    enum CodingKeys: String, CodingKey {
        case  id
        case login
        case avatarURL = "avatar_url"
      
        case type
       
        case name, company, location,  bio
 
        case followers, following
      
    }
}

// Make your struct persistable

extension User: Persistable {
   
    
    public init(managedObject: UserObject) {
        
        id = managedObject.id
        name = managedObject.name
        company = managedObject.company
        avatarURL = managedObject.avatarURL
        type = managedObject.type
        location = managedObject.location
        bio = managedObject.bio
        followers = managedObject.followers
        following = managedObject.following
        login = managedObject.login
       
    }
    public func managedObject() -> UserObject
    {
        let character = UserObject()
        character.name = name ?? ""
        character.bio = bio ?? ""
        character.following = following ?? 0
        character.followers = followers ?? 0
        character.id = id
        character.company = company ?? ""
        character.avatarURL = avatarURL ?? ""
        character.type = type ?? ""
        character.login = login
        character.location = location ?? ""
        return character
    }
}
