
import Foundation
import Alamofire


// MARK: - Remote Data Source
class RemoteDataSource {
    
    func sendRequest<T>(_ request: URLRequest , param : [String : Any] ,completion: @escaping  (Result<T>) -> Void) where T: Decodable {
        
        
        AF.request(request.url!.absoluteString, method: request.method!, parameters: param , headers: request.headers).responseJSON {
            (response) in
            
            switch response.result {
                
            case .success(let res):
                do {
                    let user = try JSONDecoder().decode(T.self, from: response.data!)
                    print(res)
                    completion(.success(data: user , code : response.response?.statusCode ?? 200 , message: ""))
                    
                } catch {
                   
                    let errorResponse = try? JSONDecoder().decode(ErrorResponse.self, from: response.data!)
                    if let statusCode = response.response?.statusCode,
                       let reason = GetFailureReason(rawValue: statusCode) {
                        print(reason)
                        
                        let appError = AppError(reason: reason, message: errorResponse?.message ?? "")
                        completion(.failure(appError))
                        
                    }
                    
                }
                
                
            case .failure(_):
                if let statusCode = response.response?.statusCode,
                   let reason = GetFailureReason(rawValue: statusCode) {
                    print(reason)
                    
                    let appError = AppError(reason: reason, message: "unknown error")
                    completion(.failure(appError))
                    
                }else{
                    completion(.failure(nil))
                }
            }
        }
    }
    
}


enum Result<T : Equatable> : Equatable{
    
    case success(data: T , code : Int , message : String)
    case failure(AppError?)
    case loading
}



